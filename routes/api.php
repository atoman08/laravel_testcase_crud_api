<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\TestModelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('test-model', [TestModelController::class, 'store'])->name('test-model.store');
Route::get('test-model', [TestModelController::class, 'index'])->name('test-model.index');
Route::get('test-model/{testModel}', [TestModelController::class, 'show'])->name('test-model.show');
Route::patch('test-model/{testModel}', [TestModelController::class, 'update'])->name('test-model.update');
Route::delete('test-model/{testModel}', [TestModelController::class, 'destroy'])->name('test-model.destroy');
