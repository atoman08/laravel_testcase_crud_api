<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\testModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TestModelController extends Controller
{
    //

    public function store(Request $request){
        $request->validate(["name" => "required", "category" => "required"]);

        $result = testModel::create($request->all());
        return $result ? response()->json($result, Response::HTTP_CREATED) : response()->json($result, Response::HTTP_NOT_ACCEPTABLE);
    }

    public function update(Request $request, testModel $testModel){
        $request->validate(["name" => "required", "category" => "required"]);

        $result = $testModel->update($request->all());
        return !is_null($result) ? response()->json($result, Response::HTTP_OK) : response()->json($result, Response::HTTP_BAD_REQUEST);
    }

    public function destroy(testModel $testModel){

         return $testModel->delete();

    }

    public function show(testModel $testModel){

        return !is_null($testModel) ? response()->json($testModel, Response::HTTP_OK) : response()->json($testModel, Response::HTTP_NOT_FOUND);

    }

    public function index(){

        $result = testModel::get();
        return $result->count() > 0 ? response()->json($result, Response::HTTP_OK) : response()->json($result, Response::HTTP_NOT_FOUND);

    }


}
