<?php

namespace Tests\Feature;

use App\Models\testModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestModelTest extends TestCase
{
    use RefreshDatabase;

    private $testModel;
    public function setup():void{
        parent::setup();
        $this->testModel = testModel::factory()->create([
            "name" => "my name",
            "category" => "my category",
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_test_model()
    {

        $testModel = testModel::factory()->make();

        $response = $this->postJson(route('test-model.store'), ['name' => $testModel->name, 'category' => $testModel->category])
                ->assertCreated()
                ->json();

        $this->assertEquals([$testModel->name, $testModel->category], [$response['name'], $response['category']]);

        $this->assertDatabaseHas('test_models', ['name' => $testModel->name, 'category' => $testModel->category]);

    }

    public function test_check_create_validation(){
        $this->postJson(route('test-model.store'), [])
                ->assertUnprocessable()
                ->assertJsonValidationErrors(["name", "category"]);
    }

    public function test_update_test_model()
    {

        $this->withoutExceptionHandling();
        $updateData = ['name' => "New name2", 'category' => 'New category 2'];
        $response = $this->patchJson(route('test-model.update', $this->testModel->id), ['name' => "New name2", 'category' => 'New category 2'] )
                ->assertOk();
        //dd($response->json());

        $this->assertDatabaseHas('test_models', ['id' => $this->testModel->id, 'name' => "New name2", 'category' => 'New category 2']);

    }

    public function test_check_update_validation(){
        $this->patchJson(route('test-model.update', $this->testModel->id),)
                ->assertUnprocessable()
                ->assertJsonValidationErrors(["name", "category"]);
    }


    public function test_show_test_model(){

        $response = $this->getJson(route('test-model.show', $this->testModel->id))
            ->assertOk()
            ->json();

        $this->assertEquals([$this->testModel->name, $this->testModel->category], [$response['name'], $response['category']]);
    }

    public function test_delete_test_model(){

        $this->deleteJson(route('test-model.destroy', $this->testModel->id))
            ->assertOk();

        $this->assertDatabaseMissing('test_models', ['id' => $this->testModel->id]);
    }


    public function test_list_test_model(){

        $response = $this->getJson(route('test-model.index'))
            ->assertOk()
            ->json();

        $this->assertEquals([$this->testModel->name, $this->testModel->category], [$response[0]['name'], $response[0]['category']]);
    }



}
